package tw.com.Dean.kotlindays

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intent.*
import tw.com.Dean.kotlindays.Models.HumanModel

class IntentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        //
        setBtnSimple()
        setBtmPassValue()
        setPassValueBackBtn()
        setBtnPassObj()
    }
    //=================
    fun setBtnSimple() {
        btn_simple_intent.setOnClickListener {
            val intentB = Intent(this,IntentBActivity::class.java)
            startActivity(intentB)

        }

    }
    //=====
    fun setBtmPassValue() {
        btn_passValue_intent.setOnClickListener {
            val intentB = Intent(this,IntentBActivity::class.java)
            intentB.putExtra("key","Hello World!")
            startActivity(intentB)
        }
    }
    //=================
    fun setPassValueBackBtn() {
        btn_pass_value_back.setOnClickListener {
            val intentB = Intent(this,IntentBActivity::class.java)

            startActivityForResult(intentB,1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            1 -> if (data != null) {
                showBackValueTextView.text = data.getStringExtra("keyBack")
            }

        }
    }

    //============
    fun setBtnPassObj() {
        btn_Pass_Obj.setOnClickListener {
            val intentB = Intent(this,IntentBActivity::class.java)
            val humanModel = HumanModel("man", "KK", 30)
            intentB.putExtra("human",humanModel)
            startActivity(intentB)
        }
    }
}