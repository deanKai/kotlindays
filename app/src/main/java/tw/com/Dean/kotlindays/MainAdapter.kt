package tw.com.Dean.kotlindays

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.mainlist_item.view.*
import tw.com.Dean.kotlindays.Models.Supplier.mainModel
import tw.com.Dean.kotlindays.Models.MainModel


class MainAdapter(val context : Context, val datas: List<MainModel>) : RecyclerView.Adapter<MainAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.mainlist_item, p0 , false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size

    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        val mainModel = mainModel[p1]
        p0.setData(mainModel, p1)
    }

    inner class MyViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        var currentDataModel: MainModel? = null
        var currentPosition: Int = 0

        init {
            itemView.setOnClickListener {
                when(currentPosition){
                    0 -> context.startActivity(Intent(context,SecondActivity::class.java))
                    1 -> context.startActivity(Intent(context,IntentActivity::class.java))
                    2 -> context.startActivity(Intent(context,MaterialActivity::class.java))
                    3 -> context.startActivity(Intent(context,BottomNavigationActivity::class.java))
                    4 -> context.startActivity(Intent(context,MenuActivity::class.java))
                }
//                val intent = Intent(context,SecondActivity::class.java)
//                context.startActivity(intent)
            }
        }

        fun setData(mainModel: MainModel?, pos: Int) {
            itemView.txvTitle.text = mainModel!!.title

            this.currentDataModel = mainModel
            this.currentPosition = pos
        }
    }
}