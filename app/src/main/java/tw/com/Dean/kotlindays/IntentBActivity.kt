package tw.com.Dean.kotlindays

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intent_b.*
import tw.com.Dean.kotlindays.Models.HumanModel

class IntentBActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_b)

        setBtnBack()
        getPassedValueFromPreActivity()
        getObjValue()
    }

    fun setBtnBack() {
        btn_back.setOnClickListener {
            backAction1()
        }
    }

    fun backAction1() {
        val data = Intent()
        data.putExtra("keyBack","I'm back")
        setResult(1,data)
        finish()

    }

    fun getPassedValueFromPreActivity() {
        val intent = intent
        val value = intent.getStringExtra("key")
        value?.let {
            show_textview.text = value
        }

    }
    fun getObjValue() {
        val humanModel = intent.getParcelableExtra<HumanModel>("human")
        humanModel?.let {
            show_textview.text = "gender:${humanModel.gender},name:${humanModel.name},and age:${humanModel.age}"
        }
    }

}