package tw.com.Dean.kotlindays.Models

data class MainModel(var title: String)

object Supplier {

	val mainModel = listOf(
        MainModel("Action Bar"),
        MainModel("Intent"),
        MainModel("Material"),
        MainModel("BottomNavigation"),
        MainModel("Menu"),
        MainModel("Programming"),
        MainModel("Talking"),
        MainModel("Swimming"),
        MainModel("Reading"),
        MainModel("Walking"),
        MainModel("Sleeping"),
        MainModel("Gaming"),
        MainModel("Programming"),
        MainModel("Talking"),
        MainModel("Swimming"),
        MainModel("Reading"),
        MainModel("Walking"),
        MainModel("Sleeping"),
        MainModel("Gaming"),
        MainModel("Programming"),
        MainModel("Talking"),
        MainModel("Swimming"),
        MainModel("Reading"),
        MainModel("Walking"),
        MainModel("Sleeping"),
        MainModel("Gaming"),
        MainModel("Programming"),
        MainModel("Talking"),
        MainModel("Swimming"),
        MainModel("Reading"),
        MainModel("Walking"),
        MainModel("Sleeping"),
        MainModel("Gaming"),
        MainModel("Programming"),
        MainModel("Talking")
	)
}
