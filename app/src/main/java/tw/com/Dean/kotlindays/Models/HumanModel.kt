package tw.com.Dean.kotlindays.Models

import android.os.Parcel
import android.os.Parcelable

data class HumanModel(var gender:String, var name:String,var age: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(gender)
        parcel.writeString(name)
        parcel.writeInt(age)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HumanModel> {
        override fun createFromParcel(parcel: Parcel): HumanModel {
            return HumanModel(parcel)
        }

        override fun newArray(size: Int): Array<HumanModel?> {
            return arrayOfNulls(size)
        }
    }

}