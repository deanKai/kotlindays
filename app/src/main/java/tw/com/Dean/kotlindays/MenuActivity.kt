package tw.com.Dean.kotlindays

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        //把menu的XML和kt做綁定
        menuInflater.inflate(R.menu.resetmenu,menu)
        return  true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        //有可能上面有多個選項的時候
        if (item?.itemId == R.id.reset) {
            Toast.makeText(this,"This is Reset",Toast.LENGTH_SHORT).show()
        }

        return true
    }

}